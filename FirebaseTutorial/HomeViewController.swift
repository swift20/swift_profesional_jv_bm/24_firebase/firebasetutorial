//
//  HomeViewController.swift
//  FirebaseTutorial
//
//  Created by Jorge Teofanes Vicuña Valle on 23/11/21.
//

import UIKit
import FirebaseAuth
enum ProviderType:String {
    case basic
}

class HomeViewController: UIViewController {

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var providerLabel: UILabel!
    @IBOutlet weak var closeSessionButton: UIButton!
    
    private let email:String
    private let provider: ProviderType
    
    init(email:String, provider:ProviderType){
        self.email = email
        self.provider = provider
        super.init(nibName: "HomeViewController", bundle: nil)
    }
    
    required init?(coder : NSCoder) {
//        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
//    required init?(coder aDecorder: NSCoder) {
//        super.init(coder: aDecorder)
//
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        emailLabel.text=email
        providerLabel.text = provider.rawValue
    }
    
    @IBAction func closeSessionButtonAction(_ sender: Any) {
        
        switch provider{
        case .basic:
            do{
                try Auth.auth().signOut()
                navigationController?.popViewController(animated: true)
            }catch{
                
            }
            
        }
    }
    


}
